import React from 'react';
import './Arrow.scss';

const arrow = (props) => {

	const setColumn = () => {
		props.onMouseOver(props.column);
	}

	const addChip = () => {
		props.onAddChip();
	}

	const color = (props.color !== null) ? props.color : 'white';
	return (
		<div className='arrow' onMouseOver={ setColumn }  onClick={ addChip }>
			<svg height="60" width="100" viewBox="0 0 100 60">
				<polygon preserveAspectRatio="none" fill={color} points="100,0 50,50 0,0 "/>
			</svg>
		</div>
	);
};

export default arrow;
