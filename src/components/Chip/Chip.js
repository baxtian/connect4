import React from 'react';
import './Chip.scss';

const chip = (props) => {

	const setColumn = () => {
		props.onMouseOver(props.column);
	}

	const addChip = () => {
		props.onAddChip();
	}

	const color = (props.color !== null) ? props.color : 'white';
	const background = (props.background !== null) ? props.background : 'blue';

	const style = {
		background: color
	}
	return (
		<div className='chip' onMouseEnter={ setColumn } onClick={ addChip } style={ style }><svg height="100" width="100" viewBox="0 0 100 100">
				<path
			       fill={background}
			       d="M 0 0 L 0 100 L 100 100 L 100 0 L 0 0 z M 50 5 A 45 45 0 0 1 95 50 A 45 45 0 0 1 50 95 A 45 45 0 0 1 5 50 A 45 45 0 0 1 50 5 z "
			       />
			</svg></div>
	);
};

export default chip;
