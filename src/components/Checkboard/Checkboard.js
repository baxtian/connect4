import React from 'react';
import uuid from 'uuid/v4';

import './Checkboard.scss';
import Chip from '../Chip/Chip';
// import Arrow from '../Arrow/Arrow';
import { useStateValue } from '../../hoc/state';
import Modal from '../UI/Modal/Modal';
import Button from '../UI/Button/Button';

const Checkboard = (props) => {
	//Variables
	const [context, dispatch] = useStateValue();

	//Funciones
	const addChip = () => {
		// Si hay un ganador no hacer nada
		if(context.winner !== null) return;

		// Si no hay una columna seleccionada no hacer nada
		if(context.selectedCol === null) return;

		let i = 0;
		// Recorrer la columna fila por fila hasta encontrar un espacio ocupado o hasta llegar al final
		for(i=0; i<context.rows; i++) {
			if(context.map[i][context.selectedCol] !== 'X') break;
		}

		// Agregar en la fila anterior al límite encontrado
		// y cambiar el turno
		i--;
		if(i>=0) {
			// Modificar el mapa
			let _map = [...context.map];
			_map[i][context.selectedCol] = (context.turn === 'red') ? 'R' : 'Y';
			dispatch( {
				type: 'setMap',
				newMap: _map
			} )

			// Determinar si hay ganador
			if(hasWinner(context.selectedCol, i)) {
				dispatch( {
					type: 'setWinner',
					newWinner: context.turn
				} )
			} else {
				// Cambiar el turno
				if(context.turn === 'red') dispatch( {
					type: 'changeTurn',
					newTurn: 'gold'
				} );
				else dispatch( {
					type: 'changeTurn',
					newTurn: 'red'
				} );
			}

			// Ocultar la flecha si ya no hay espacio en la columna
			if(i === 0) dispatch( {
				type: 'setSelectedCol',
				newSelectedCol: null
			} );
		}
	};

	const hasWinner = (col, row) => {
		// Un ganador se determina por tener 4 del mismo tipo seguidos. Se recorrerá
		// cada dirección completa que pase por este punto y si hay cuatro seguidos se da
		// como ganador
		let i = 0;
		let count = 0;
		let search = (context.turn === 'red') ? 'R' : 'Y';
		let _col = col;
		let _row = row;

		// Revisar horizontalmente
		for(i=0; i<context.columns; i++) {
			if(context.map[row][i] === search) {
				count++;
			} else {
				count = 0;
			}
			if(count === 4) return true;
		}

		// Revisar verticalmente
		count = 0;
		for(i=0; i<context.rows; i++) {
			if(context.map[i][col] === search) {
				count++;
			} else {
				count = 0;
			}
			if(count === 4) return true;
		}

		// Revisar diagonal 1
		_col = col;
		_row = row;
		count = 0;
		// Buscar el inicio de la diagonal
		while(_col !== 0 && _row !==0) {
			_col--;
			_row--;
		}
		while(_col !== context.columns && _row !== context.rows) {
			if(context.map[_row][_col] === search) {
				count++;
			} else {
				count = 0;
			}
			if(count === 4) return true;
			_col++;
			_row++;
		}

		// Revisar diagonal 2
		_col = col;
		_row = row;
		count = 0;
		// Buscar el inicio de la diagonal
		while(_col !== 0 && _row !==context.rows-1) {
			_col--;
			_row++;
		}
		while(_col !== context.columns && _row !== -1) {
			if(context.map[_row][_col] === search) {
				count++;
			} else {
				count = 0;
			}
			if(count === 4) return true;
			_col++;
			_row--;
		}

		// No encontramos ganador
		return false;
	}

	const top = [
			Array.from({
				length: context.columns
			}, (v, i) => {
				const color = ( i === context.selectedCol) ? context.turn : 'white';
				return <Chip key={ uuid() } background='white' color={color} column={ i } onMouseOver={ () => dispatch( {
					type: 'setSelectedCol',
					newSelectedCol: i
				} ) } onAddChip={ addChip } />;
			}),
		];

	const info = (context.winner) ? <div key={ uuid() } className='full chip' onClick={ () => dispatch( { type: 'reset' } ) }>Winner<br /><strong>{context.turn}</strong></div> : <Chip key={ uuid() } color={context.turn} column={ null } onMouseOver={ () => dispatch( {
		type: 'setSelectedCol',
		newSelectedCol: null
	} ) } onAddChip={ addChip } background='DarkSlateGray ' />;

	const board = context.map.map((row, i, arr) => {
		return (<>
			{
				row.map((item, i) => {
					let color = null;
					if (item === 'Y')
						color = 'gold';
					else if (item === 'R')
						color = 'red';
					return <Chip key={ uuid() } color={color} background='blue' column={ i } onMouseOver={ () => dispatch( {
						type: 'setSelectedCol',
						newSelectedCol: i
					} ) } onAddChip={ addChip }/>;
				})
			}
		</>)
	});


	const modal = [
		<h1>Winner: <span className={ context.winner }>{context.winner}</span></h1>,
		<Button btnType="danger" clicked={() => dispatch( {
			type: 'reset'
		} )}>Clear board</Button>];
	return (<div className='checkboard'>
		<Modal show={context.winner}>
			{modal}
		</Modal>
		<div className='board'>
			{top}
			{board}
		</div>
	</div>);
};

export default Checkboard;
