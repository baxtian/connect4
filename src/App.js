import React from 'react';
import {StateProvider} from './hoc/state';
import './App.scss';

import Checkboard from './components/Checkboard/Checkboard';

const App = () => {
	const initialState = {
		turn: 'red',
		map: [],
		winner: null,
		rows: 6,
		columns: 7
	};

	// Inicializar mapa
	initialState.map = Array.from({
		length: initialState.rows
	}, () => new Array(initialState.columns).fill('X'));

	const reducer = (state, action) => {
		// console.log(state);
		// console.log(action);
		switch (action.type) {
			case 'changeTurn':
				return {
					...state,
					turn: action.newTurn
				};
			case 'setSelectedCol':
				let value = action.newSelectedCol;
				if( value !== null) {
					if(state.map[0][value] !== 'X')
						value = null;
				}
				if(state.winner !== null) { value = null; }

				return {
					...state,
					selectedCol: value
				};
			case 'setMap':
				return {
					...state,
					map: action.newMap
				};
			case 'setWinner':
				return {
					...state,
					winner: action.newWinner
				};
			case 'reset':
				return {
					...state,
					map: Array.from({
							length: state.rows
						}, () => new Array(state.columns).fill('X')),
					turn: 'red',
					winner: null
				};
			default:
				return state;
		}
	};

	return (<StateProvider initialState={initialState} reducer={reducer}>
		<Checkboard/>
	</StateProvider>);
}

export default App;
